package com.example.trafficlight.trafficlight

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.trafficlight.R
import com.example.trafficlight.common.utils.repeat
import com.example.trafficlight.model.TrafficLightState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class TrafficLightViewModel : ViewModel() {

    val trafficLightStates = listOf(
        TrafficLightState(R.color.red, 4_000L),
        TrafficLightState(R.color.orange, 1_000L),
        TrafficLightState(R.color.green, 4_000L)
    )

    private val _currentTrafficLightState: MutableStateFlow<TrafficLightState> =
        MutableStateFlow(trafficLightStates.first())
    val currentTrafficLightState: StateFlow<TrafficLightState>
        get() = _currentTrafficLightState

    private var job: Job? = null

    init {
        startTrafficLight()
    }

    private fun startTrafficLight() {
        viewModelScope.launch {
            job?.cancel()
            job = launch {

                val trafficLightStatesSequenceIterator = trafficLightStates
                    .asSequence()
                    .repeat()
                    .iterator()

                while (trafficLightStatesSequenceIterator.hasNext()) {
                    val nextTrafficLightState = trafficLightStatesSequenceIterator.next()
                    _currentTrafficLightState.value = nextTrafficLightState
                    delay(nextTrafficLightState.duration)
                }
            }
        }
    }

    fun cancelAllJobs() {
        job?.cancel()
        job = null
    }

    override fun onCleared() {
        super.onCleared()
        cancelAllJobs()
    }
}