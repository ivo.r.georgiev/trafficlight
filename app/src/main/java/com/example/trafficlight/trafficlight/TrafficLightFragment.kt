package com.example.trafficlight.trafficlight

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.example.trafficlight.databinding.FragmentTrafficLightBinding
import kotlinx.coroutines.launch

class TrafficLightFragment : Fragment() {

    private var _binding: FragmentTrafficLightBinding? = null
    private val binding get() = _binding!!

    private val args: TrafficLightFragmentArgs by navArgs()

    private val viewModel by viewModels<TrafficLightViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTrafficLightBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()

    }

    private fun initViews() {
        binding.txtCarModel.text = args.car.model
        binding.trafficLight.setupTrafficLight(viewModel.trafficLightStates)
    }

    private fun initObservers() {
        lifecycleScope.launch {
            viewModel.currentTrafficLightState.collect {
                binding.trafficLight.updateState(it)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}