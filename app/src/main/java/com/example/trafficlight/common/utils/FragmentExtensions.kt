package com.example.trafficlight.common.utils

import androidx.core.util.Consumer
import androidx.fragment.app.Fragment
import com.example.trafficlight.common.liveevent.LiveEvent

fun<T> Fragment.consume(liveEvent: LiveEvent<T>, consumer: Consumer<T>) {
    liveEvent.consume(viewLifecycleOwner, consumer)
}