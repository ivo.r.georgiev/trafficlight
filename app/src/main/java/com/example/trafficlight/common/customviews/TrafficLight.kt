package com.example.trafficlight.common.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.core.view.children
import com.example.trafficlight.R
import com.example.trafficlight.common.utils.animateAlpha
import com.example.trafficlight.common.utils.dp
import com.example.trafficlight.common.utils.setTint
import com.example.trafficlight.databinding.TrafficLightViewBinding
import com.example.trafficlight.model.TrafficLightState

class TrafficLight @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    private var _binding: TrafficLightViewBinding? = null
    private val binding
        get() = _binding!!

    init {
        _binding = TrafficLightViewBinding.inflate(
            LayoutInflater.from(context),
            this,
            true
        )
    }

    fun setupTrafficLight(trafficLightStatesList: List<TrafficLightState>) {
        with(binding) {
            trafficLightStatesList.forEach { state ->
                val ledView = ImageView(context)
                ledView.apply {
                    setImageResource(R.drawable.ic_circle)
                    val layoutParams = LinearLayout.LayoutParams(
                        80.dp,
                        80.dp
                    )
                    this.layoutParams = layoutParams
                    tag = state.color
                    setTint(state.color)
                    alpha = 0.3f
                }
                grpTrafficLight.addView(ledView)
            }
        }
    }

    fun updateState(state: TrafficLightState) {
        binding.grpTrafficLight.children.forEach {
            if (it is ImageView) {
                val ledAlpha = if (it.tag == state.color) 1f else 0.3f
                it.animateAlpha(ledAlpha)
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        _binding = null
    }
}