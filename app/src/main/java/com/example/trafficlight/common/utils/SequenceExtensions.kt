package com.example.trafficlight.common.utils

fun <T> Sequence<T>.repeat() = sequence { while (true) yieldAll(this@repeat) }