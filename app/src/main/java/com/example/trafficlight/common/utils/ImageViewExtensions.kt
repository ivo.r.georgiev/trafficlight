package com.example.trafficlight.common.utils

import android.view.animation.BaseInterpolator
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

fun ImageView.setTint(@ColorRes color: Int) {
    setColorFilter(
        ContextCompat.getColor(
            context,
            color
        )
    )
}

fun ImageView.animateAlpha(
    alpha: Float,
    duration: Long = 300L,
    interpolator: BaseInterpolator = LinearInterpolator()
) {
    animate().apply {
        this.interpolator = interpolator
        this.duration = duration
        alpha(alpha)
        start()
    }
}