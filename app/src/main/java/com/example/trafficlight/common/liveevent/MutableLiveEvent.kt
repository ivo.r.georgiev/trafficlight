package com.example.trafficlight.common.liveevent

import androidx.annotation.MainThread

class MutableLiveEvent<T> : LiveEvent<T>() {

    override fun postValue(value: Event<T>?) {
        super.postValue(value)
    }

    @MainThread
    override fun setValue(value: Event<T>?) {
        super.setValue(value)
    }


    @MainThread
    @JvmOverloads
    fun fire(e :T? = null) {
        value = Event(e)
    }
}