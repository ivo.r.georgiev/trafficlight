package com.example.trafficlight.dealership

import androidx.lifecycle.ViewModel
import com.example.trafficlight.common.liveevent.LiveEvent
import com.example.trafficlight.common.liveevent.MutableLiveEvent
import com.example.trafficlight.model.FormResult
import com.example.trafficlight.model.Car

class DealershipViewModel : ViewModel() {

    private var _shouldNavigateToTrafficLightFragment = MutableLiveEvent<Car>()
    val shouldNavigateToTrafficLightFragment: LiveEvent<Car>
        get() = _shouldNavigateToTrafficLightFragment

    private var _shouldShowErrorMessage = MutableLiveEvent<FormResult.Error>()
    val shouldShowErrorMessage: LiveEvent<FormResult.Error>
        get() = _shouldShowErrorMessage

    fun onStartDrivingButtonClicked(carModel: String) {
        val trimmedCarModel = carModel.trim()

        when (val formResult = validateCarModelInput(trimmedCarModel)) {
            is FormResult.Error -> _shouldShowErrorMessage.fire(formResult)
            is FormResult.Valid -> _shouldNavigateToTrafficLightFragment.fire(Car(trimmedCarModel))
        }
    }

    private fun validateCarModelInput(carModel: String): FormResult {
        val tempCarModel = carModel.trim()
        return when {
            tempCarModel.isBlank() -> FormResult.Error.EmptyInput
            tempCarModel.length < 3 -> FormResult.Error.InputTooShort
            else -> FormResult.Valid
        }
    }
}