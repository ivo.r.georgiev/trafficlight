package com.example.trafficlight.dealership

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.trafficlight.common.utils.consume
import com.example.trafficlight.databinding.FragmentDealershipBinding

class DealershipFragment : Fragment() {

    private var _binding: FragmentDealershipBinding? = null
    private val binding get() = _binding!!

    private val dealershipViewModel by viewModels<DealershipViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDealershipBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        initObservers()
    }

    private fun setupViews() {
        with(binding) {
            btnStartDriving.setOnClickListener {
                dealershipViewModel.onStartDrivingButtonClicked(binding.txtCarModel.text.toString())
            }
        }
    }

    private fun initObservers() {
        consume(dealershipViewModel.shouldNavigateToTrafficLightFragment) { car ->
            binding.txtCarModel.text?.clear()
            val action = DealershipFragmentDirections.navigateToTrafficLightFragment(car)
            findNavController().navigate(action)
        }

        consume(dealershipViewModel.shouldShowErrorMessage) { errorMessage ->
            Toast.makeText(
                binding.root.context,
                binding.root.context.getString(errorMessage.errorMessage),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}