package com.example.trafficlight.model

import androidx.annotation.StringRes
import com.example.trafficlight.R

sealed class FormResult {
    data object Valid : FormResult()
    sealed class Error(@StringRes val errorMessage: Int) : FormResult() {
        data object EmptyInput : Error(R.string.empty_form_error)
        data object InputTooShort : Error(R.string.form_minimal_character_length_error)
    }
}