package com.example.trafficlight.model

data class TrafficLightState(
    val color: Int,
    val duration: Long,
)
