package com.example.trafficlight.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Car(val model: String) : Parcelable