package com.example.trafficlight.trafficlight

import com.example.trafficlight.MainCoroutineRule
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runCurrent
import org.junit.Before
import com.example.trafficlight.R
import com.example.trafficlight.model.TrafficLightState
import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)

@RunWith(JUnit4::class)
class TrafficLightViewModelTest {

    private lateinit var viewModel: TrafficLightViewModel

    private val redTrafficLightState = TrafficLightState(R.color.red, 4_000L)
    private val orangeTrafficLightState = TrafficLightState(R.color.orange, 1_000L)
    private val greenTrafficLightState = TrafficLightState(R.color.green, 4_000L)

    @get:Rule
    val dispatcher = MainCoroutineRule()

    @Before
    fun setUp() {
        viewModel = TrafficLightViewModel()
    }

    @Test
    fun `verify traffic light transition from red to orange light`() = runTest {
        assertEquals(redTrafficLightState, viewModel.currentTrafficLightState.value)

        advanceTimeBy(redTrafficLightState.duration)
        runCurrent()

        viewModel.cancelAllJobs()
        assertEquals(orangeTrafficLightState, viewModel.currentTrafficLightState.value)
    }

    @Test
    fun `verify traffic light transition from red to green light`() = runTest {
        assertEquals(redTrafficLightState, viewModel.currentTrafficLightState.value)

        advanceTimeBy(
            redTrafficLightState.duration +
                    orangeTrafficLightState.duration
        )
        runCurrent()

        viewModel.cancelAllJobs()
        assertEquals(greenTrafficLightState, viewModel.currentTrafficLightState.value)
    }

    @Test
    fun `verify traffic light transition cycle from and back to red light`() = runTest {
        assertEquals(redTrafficLightState, viewModel.currentTrafficLightState.value)

        advanceTimeBy(
            redTrafficLightState.duration +
                    orangeTrafficLightState.duration +
                    greenTrafficLightState.duration
        )
        runCurrent()

        viewModel.cancelAllJobs()
        assertEquals(redTrafficLightState, viewModel.currentTrafficLightState.value)
    }

    @Test
    fun `verify individual transitions cycled from and back to red light`() = runTest {
        assertEquals(redTrafficLightState, viewModel.currentTrafficLightState.value)

        advanceTimeBy(redTrafficLightState.duration)
        runCurrent()
        assertEquals(orangeTrafficLightState, viewModel.currentTrafficLightState.value)

        advanceTimeBy(orangeTrafficLightState.duration)
        runCurrent()
        assertEquals(greenTrafficLightState, viewModel.currentTrafficLightState.value)

        advanceTimeBy(greenTrafficLightState.duration)
        runCurrent()
        viewModel.cancelAllJobs()
        assertEquals(redTrafficLightState, viewModel.currentTrafficLightState.value)
    }
}