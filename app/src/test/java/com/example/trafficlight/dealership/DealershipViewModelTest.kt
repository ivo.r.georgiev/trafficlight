package com.example.trafficlight.dealership

import android.os.Looper
import com.example.trafficlight.model.Car
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Before
import com.example.trafficlight.model.FormResult
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DealershipViewModelTest {

    private val viewModel = DealershipViewModel()

    @Before
    fun setUp() {
        mockkStatic(Looper::class)
        val looper = mockk<Looper> {
            every { thread } returns Thread.currentThread()
        }

        every { Looper.getMainLooper() } returns looper
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `enter empty car model resulting in empty input error`() {
        viewModel.onStartDrivingButtonClicked("")
        assertEquals(
            FormResult.Error.EmptyInput,
            viewModel.shouldShowErrorMessage.value?.value
        )
    }

    @Test
    fun `enter short car model resulting in input too short error`() {
        viewModel.onStartDrivingButtonClicked("au")
        assertEquals(
            FormResult.Error.InputTooShort,
            viewModel.shouldShowErrorMessage.value?.value
        )
    }

    @Test
    fun `enter valid car model resulting in success`() {
        val car = Car("Kia")
        viewModel.onStartDrivingButtonClicked(car.model)
        assertEquals(
            car,
            viewModel.shouldNavigateToTrafficLightFragment.value?.value
        )
    }

    @Test
    fun `enter blank car model resulting in empty input error`() {
        viewModel.onStartDrivingButtonClicked("    ")
        assertEquals(
            FormResult.Error.EmptyInput,
            viewModel.shouldShowErrorMessage.value?.value
        )
    }
}